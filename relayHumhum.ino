
// Include Libraries
#include "Arduino.h"
#include "DHT.h"
#include "LiquidCrystal_PCF8574.h"
#include "Relay.h"

// Pin Definitions
#define DHT_PIN_DATA	2
#define RELAYMODULE_PIN_SIGNAL  8

#define LCD_ADDRESS 0x3F
#define LCD_ROWS 2
#define LCD_COLUMNS 16
#define SCROLL_DELAY 150
#define BACKLIGHT 255
// object initialization
DHT dht(DHT_PIN_DATA);
LiquidCrystal_PCF8574 lcdI2C;
Relay relayModule(RELAYMODULE_PIN_SIGNAL);

// define vars for testing menu
const int timeout = 5000;       //define timeout of 10 sec
char menuOption = 0;
long time0;


// Setup the essentials for your circuit to work. It runs first every time your circuit is powered with electricity.
void setup() 
{
    
    Serial.begin(9600);
    while (!Serial) ; // wait for serial port to connect. Needed for native USB
    Serial.println("start");
    
    dht.begin();
    // initialize the lcd
    lcdI2C.begin(LCD_COLUMNS, LCD_ROWS, LCD_ADDRESS, BACKLIGHT); 
    //menuOption = menu();
   // pinMode(8,OUTPUT);
    //digitalWrite(8,LOW);
}

// Main logic of your circuit. It defines the interaction between the components you selected. After setup, it runs over and over again, in an eternal loop.
void loop() 
{
    
    // mao ni akong program
    //if(menuOption == '1') {
    // DHT22/11 Humidity and Temperature Sensor - Test Code
    // Reading humidity in %
    float dhtHumidity = dht.readHumidity();
    // Read temperature in Celsius, for Fahrenheit use .readTempF()
    float dhtTempC = dht.readTempC();
    Serial.print(F("Humidity: ")); Serial.print(dhtHumidity); Serial.print(F(" [%]\t"));
    Serial.print(F("Temp: ")); Serial.print(dhtTempC); Serial.println(F(" [C]"));
    lcdI2C.clear();                          // Clear LCD screen.
    
    if (dhtHumidity <= 48){
      showlcdnc();
      Serial.print("Motor is on");
      delay(1000);
      relayModule.off(); 
      showlcdnc();     
      delay(500);             
      relayModule.on();     
      delay(5000);
      showlcdnc();
    }
    else if (dhtHumidity >53 ){
      showlcd();
      relayModule.on();
      //Serial.print(" ");
    }
    
    
    lcdI2C.print("Humidity:");   lcdI2C.print(dhtHumidity);          
    lcdI2C.selectLine(2);                  
    lcdI2C.print("Temperature: "); lcdI2C.print(dhtTempC);                  
    delay(2000);
    
 //   }
 //   else if(menuOption == '2') {
    // LCD 16x2 I2C - Test Code
    // The LCD Screen will display the text of your choice.
 //   lcdI2C.clear();                          // Clear LCD screen.
    
 //   lcdI2C.print("  Rod Ryan ");                   // Print print String to LCD on first line
//lcdI2C.selectLine(2);                    // Set cursor at the begining of line 2
  //  lcdI2C.print("Mendoza is here  ");                   // Print print String to LCD on second line
  //  lcdI2C.scrollDisplayLeft(16,1);
    
  //  delay(1000);

  //  }
    
   // if (millis() - time0 > timeout)
   // {
   //     menuOption = menu();
  //  }
    
}  // end of code sa body

char showlcd() 
{
  float dhtHumidity = dht.readHumidity();
    // Read temperature in Celsius, for Fahrenheit use .readTempF()
  float dhtTempC = dht.readTempC();
  Serial.print(F("Humidity: ")); Serial.print(dhtHumidity); Serial.print(F(" [%]\t"));
  Serial.print(F("Temp: ")); Serial.print(dhtTempC); Serial.println(F(" [C]"));
  lcdI2C.clear();
}
char showlcdnc() 
{
  float dhtHumidity = dht.readHumidity();
    // Read temperature in Celsius, for Fahrenheit use .readTempF()
  float dhtTempC = dht.readTempC();
  Serial.print(F("Humidity: ")); Serial.print(dhtHumidity); Serial.print(F(" [%]\t"));
  Serial.print(F("Temp: ")); Serial.print(dhtTempC); Serial.println(F(" [C]"));
  //lcdI2C.clear();
}
// Menu function for selecting the components to be tested
// Follow serial monitor for instrcutions
char menu()
{

    Serial.println(F("\nWhich component would you like to test?"));
    Serial.println(F("(1) DHT22/11 Humidity and Temperature Sensor"));
    Serial.println(F("(2) LCD 16x2 I2C"));
    Serial.println(F("(menu) send anything else or press on board reset button\n"));
    while (!Serial.available());

    // Read data from serial monitor if received
    while (Serial.available()) 
    {
        char c = Serial.read();
        if (isAlphaNumeric(c)) 
        {   
            
            if(c == '1') 
    			Serial.println(F("Now Testing DHT22/11 Humidity and Temperature Sensor"));
    		else if(c == '2') 
    			Serial.println(F("Now Testing LCD 16x2 I2C"));
            else
            {
                Serial.println(F("illegal input!"));
                return 0;
            }
            time0 = millis();
            return c;
        }
    }
}
